# object oriented programming

Here we simulate a virtual world populated by 100,000 people. We seek to know:
- from what population density people are less pleasant than the average,
- from what age people earn more money than the average.

When the program launches, a first window displays a graph concerning the population density then a second concerning the income.

# if you get this error: No module named 'tkinter'

    sudo apt-get install python3-tk

# dependencies install

Pour tracer des graphes, vous allez avoir besoin d'installer `matplotlib` :

    pip install matplotlib

# Datas

The 100,000 agents are courtesy of [PPLAPI](http://pplapi.com).
*Remember to unzip the document!*

    unzip agents-100k.zip

If you want to generate new ones, enter the following command:

    ./download_agents -d agents-100k.json -c 100000

# Launch

    ./model.py agents-100k.json
